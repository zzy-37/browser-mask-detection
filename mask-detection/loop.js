class Loop {
    constructor(onframe) {
        this.onframe = onframe
    }

    #looping = false
    get looping() {
        return this.#looping
    }

    #next = async ts => {
        if (this.#looping) {
            if (this.onframe) await this.onframe(ts)
            requestAnimationFrame(this.#next)
        }
    }

    start() {
        if (!this.#looping) {
            this.#looping = true
            requestAnimationFrame(this.#next)
        }
    }

    stop() {
        this.#looping = false
    }
}