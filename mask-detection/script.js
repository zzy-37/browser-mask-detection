const modules = {
    face_detection: null,
    mask_detection: null,
}

const globals = {
    video: document.createElement('video'),
    rects: []
}

async function init_face_detection() {
    const fd = new FaceDetection({
        locateFile: file => `https://cdn.jsdelivr.net/npm/@mediapipe/face_detection@0.4/${file}`
    })

    fd.setOptions({
        selfieMode: false,
        model: 'short',
        minDetectionConfidence: 0.5,
    })

    fd.onResults(handle_face_detection_results)

    await fd.initialize()

    modules.face_detection = fd
}

async function init_mask_detection() {
    const url = "https://teachablemachine.withgoogle.com/models/3Ir-mAsDp/"
    const model_url = url + 'model.json'
    const metadata_url = url + 'metadata.json'
    modules.mask_detection = await tmImage.load(model_url, metadata_url)
}

init_face_detection()
init_mask_detection()

function handle_face_detection_results(results) {
    globals.rects = results.detections.map(d => {
        const w = Math.floor(globals.video.videoWidth  * d.boundingBox.width)
        const h = Math.floor(globals.video.videoHeight * d.boundingBox.height)
        const x = Math.floor(globals.video.videoWidth  * d.boundingBox.xCenter - w / 2)
        const y = Math.floor(globals.video.videoHeight * d.boundingBox.yCenter - h / 2)
        return { x, y, w, h }
    })

    update_mask_detection_results()
}

const update_mask_detection_results = (function () {
    const result_container = document.querySelector('#mask-detection-result-container')
    const face_cnv = document.createElement('canvas')
    const face_ctx = face_cnv.getContext('2d')

    function gen_element_from_prediction(prediction, i) {
        const result = document.createElement('div')
        const heading = document.createElement('h3')
        heading.textContent = `Result ${i}`

        const records = prediction.map(record => {
            const elem = document.createElement('div')
            elem.textContent = `${record.className}: ${record.probability.toFixed(2)}`
            return elem
        })

        result.append(heading, ...records)

        return result
    }

    const map_async = (arr, callback) => Promise.all(arr.map(callback))

    return async function () {
        if (modules.mask_detection) {
            const result_elements = await map_async(globals.rects, async ({ x, y, w, h }, i) => {
                face_cnv.width  = w
                face_cnv.height = h
                face_ctx.drawImage(globals.video, x, y, w, h, 0, 0, w, h)

                const prediction = await modules.mask_detection.predict(face_cnv)

                return gen_element_from_prediction(prediction, i + 1)
            })

            result_container.replaceChildren(...result_elements)
        }
    }
})()

function load_mask_detection_app() {
    const cnv = document.querySelector('#mask-detection-app canvas')
    const ctx = cnv.getContext('2d')

    const start_stop_button = document.querySelector('#mask-detection-start-stop-button')
    const mirrored_checkbox = document.querySelector('#mask-detection-mirror-checkbox')

    const mirrored = () => mirrored_checkbox.checked

    function ctx_draw_mirrored(callback) {
        ctx.save()
        if (mirrored()) {
            ctx.translate(cnv.width, 0)
            ctx.scale(-1, 1)
        }
        callback()
        ctx.restore()
    }

    function draw(ts) {
        ctx_draw_mirrored(() => {
            ctx.drawImage(globals.video, 0, 0, cnv.width, cnv.height)
            ctx.lineWidth = 3
            ctx.strokeStyle = 'blue'
            globals.rects.map(({ x, y, w, h }) => {
                ctx.strokeRect(x, y, w, h)
            })
        })

        if (modules.face_detection)
            modules.face_detection.send({ image: globals.video })
    }

    start_stop_button.textContent = 'Start'
    start_stop_button.onclick = start_camera

    async function start_camera() {
        start_stop_button.disabled = true

        const stream = await navigator.mediaDevices.getUserMedia({ video: true })
        globals.video.autoplay = true

        globals.rects = []
        const loop = new Loop(draw)

        function stop_camera() {
            loop.stop()
            ctx.clearRect(0, 0, cnv.width, cnv.height)

            for (const track of stream.getTracks())
                track.stop()

            start_stop_button.textContent = 'Start'
            start_stop_button.onclick = start_camera
        }

        globals.video.onloadedmetadata = () => {
            cnv.width  = globals.video.videoWidth
            cnv.height = globals.video.videoHeight

            loop.start()

            start_stop_button.textContent = 'Stop'
            start_stop_button.onclick = stop_camera
            start_stop_button.disabled = false
        }

        globals.video.srcObject = stream
    }
}

load_mask_detection_app()