const videoElem = document.querySelector('#qrcode-scanner-content video')
const iframe = document.querySelector('#qrcode-scanner-content iframe')
const camera_button = document.querySelector('#qrcode-camera-button')

const current_page = () => iframe.src

const qrScanner = new QrScanner(
    videoElem,
    result => {
        if (result.data !== current_page()) {
            console.log(result)
            iframe.src = result.data
        }
    },
    { /* your options or returnDetailedScanResult: true if you're not specifying any other options */
        highlightScanRegion: true },
)

camera_button.textContent = 'Start'
camera_button.onclick = function () {
    if (this.textContent === 'Start') {
        qrScanner.start()
        this.textContent = 'Stop'
    } else {
        qrScanner.stop()
        this.textContent = 'Start'
    }
}