function initialize_start_stop_button(on_media_stream_load_callback) {
    const start_stop_button = document.querySelector('#start-stop-button')

    start_stop_button.init = () => {
        start_stop_button.onclick = start_camera
        start_stop_button.textContent = 'Start'
    }

    start_stop_button.init()

    async function start_camera() {
        start_stop_button.disabled = true

        const stream = await navigator.mediaDevices.getUserMedia({ video: true })

        video_elem.onloadedmetadata = () => {
            if (on_media_stream_load_callback) on_media_stream_load_callback()

            start_stop_button.onclick = () => {
                stream.getTracks().forEach(track => track.stop())
                start_stop_button.init()
            }
            start_stop_button.textContent = 'Stop'
            start_stop_button.disabled = false
        }

        video_elem.srcObject = stream
    }
}

const video_elem = document.createElement('video')

const flipped = () => document.querySelector('#flip-image-checkbox').checked
const get_frame = () => (flipped()) ? ml5.flipImage(video_elem) : video_elem

initialize_start_stop_button(() => {
    console.log("Camera loaded.")

    const frame = get_frame()
    console.log(frame)
    
    faceapi.detectSingle(frame, (err, results) => {
        console.log(results)
    })
})

const faceapi = ml5.faceApi({
    withDescriptors: false,
    withLandmarks: false,
    withTinyNet: false
}, () => {
    console.log('Model Loaded!')
})